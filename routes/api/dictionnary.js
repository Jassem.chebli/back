const express = require("express");
const router = express.Router();
const keys = require("../../config/keys");
// Load Dictionnary model
const Dictionnary = require("../../models/Dictionnary");

// @route POST api/dictionnary/ajouterWord
// @desc add word to dictionnary
// @access Private
router.post("/ajouterWord/:word", (req, res) => {
    Dictionnary.findOne({ word: req.params.word }).then(dictionnary => {
        if (dictionnary) {
            return res.status(400).json({ word: "word already exists" });
        } else {
            const newDictionnary = new Dictionnary({
                word: req.params.word,
                synonyme: []
            });
            newDictionnary
                .save()
                .then(dictionnary => res.json(dictionnary))
                .catch(err => console.log(err));
        }
    });
});

// @route DELETE api/dictionnary/deleteSynonyme
// @desc delete a synonyme of word for dictionnairy
// @access Private
router.delete("/deleteSynonyme/:word/:synonyme", (req, res) => {
    console.log(req.params.synonyme)
    Dictionnary.findOne({word: req.params.word})
        .then(dictionnary => {
            console.log(dictionnary);
            dictionnary.synonyme.splice(dictionnary.synonyme.indexOf(req.params.synonyme),1); //delete the synonyme from list synonyme
            Dictionnary.updateMany({word: req.params.word} , {$set :{word : req.params.word , synonyme:dictionnary.synonyme}})
            .then(dictionnary => res.json(dictionnary))
            .catch(err => console.log(err));}
        )
        .catch(err => console.log(err));
});

// @route POST api/dictionnary/AjouterSynonyme
// @desc modifier synonyme of word for dictionnairy
// @access Private
router.post("/AjouterSynonyme/:word/:synonyme", (req, res) => {
    console.log(req.params.synonyme)
    Dictionnary.findOne({word: req.params.word})
        .then(dictionnary => {
            console.log(dictionnary);
            dictionnary.synonyme.push(req.params.synonyme) //add the synonyme to list synonyme
            Dictionnary.updateMany({word: req.params.word} , {$set :{word : req.params.word , synonyme:dictionnary.synonyme}})
            .then(dictionnary => res.json(dictionnary))
            .catch(err => console.log(err));}
        )
        .catch(err => console.log(err));
});

// @route DELETE api/dictionnary/supprimer
// @desc delete word for dictionnairy
// @access Private
router.delete("/supprimer/:word", (req, res) => {
    Dictionnary.remove({word: req.params.word})
                .then(dictionnary => res.json(dictionnary))
                .catch(err => console.log(err));
});

// @route PUT api/dictionnary/updateWord
// @desc modifier synonyme of word for dictionnairy
// @access Private
router.put("/updateWord/:word/:newWord", (req, res) => {
    Dictionnary.findOne({word: req.params.word})
        .then(dictionnary => {
            Dictionnary.updateMany({word: req.params.word} , {$set :{word : req.params.newWord}})
            .then(dictionnary => res.json(dictionnary))
            .catch(err => console.log(err));}
        )
        .catch(err => console.log(err));
});

// @route GET api/dictionnary/
// @desc get all word from dictionnairy
// @access Private
router.get("/", (req, res) => {
    Dictionnary.find({})
                .then(dictionnary => res.json(dictionnary))
                .catch(err => console.log(err));
});

// @route GET api/dictionnary/rechercher
// @desc find all synomyme of a word from dictionnairy
// @access Private
router.get("/rechercher", (req, res) => {
    Dictionnary.find({word: req.body.word})
                .then(dictionnary => res.json(dictionnary))
                .catch(err => console.log(err));
});

module.exports = router;