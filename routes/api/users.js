const express = require("express");
const router = express.Router();
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const keys = require("../../config/keys");
// Load input validation
const validateRegisterInput = require("../../validation/register");
const validateLoginInput = require("../../validation/login");
// Load User model
const User = require("../../models/User");
const Descriptors = require("../../models/Descriptors");

// @route POST api/users/register
// @desc Register user
// @access Public
router.post("/register", (req, res) => {
    // Form validation
    const { errors, isValid } = validateRegisterInput(req.body);
    // Check validation
    if (!isValid) {
        return res.status(400).json(errors);
    }
    User.findOne({ email: req.body.email }).then(user => {
        if (user) {
            return res.status(400).json({ email: "Email already exists" });
        } else {
            const newUser = new User({
                name: req.body.name,
                email: req.body.email,
                password: req.body.password
            });
            // Hash password before saving in database
            bcrypt.genSalt(10, (err, salt) => {
                bcrypt.hash(newUser.password, salt, (err, hash) => {
                    if (err) throw err;
                    newUser.password = hash;
                    newUser
                        .save()
                        .then(user => res.json(user))
                        .catch(err => console.log(err));
                });
            });
        }
    });
});

// @route POST api/users/login
// @desc Login user and return JWT token
// @access Public
router.post("/login", (req, res) => {
    // Form validation
    const { errors, isValid } = validateLoginInput(req.body);
    // Check validation
    if (!isValid) {
        return res.status(400).json(errors);
    }
    const email = req.body.email;
    const password = req.body.password;
    // Find user by email
    User.findOne({ email }).then(user => {
        // Check if user exists
        if (!user) {
            return res.status(404).json({ emailnotfound: "Email not found" });
        }
        // Check password
        bcrypt.compare(password, user.password).then(isMatch => {
            if (isMatch) {
                // User matched
                // Create JWT Payload
                const payload = {
                    id: user.id,
                    name: user.name
                };
                // Sign token
                jwt.sign(
                    payload,
                    keys.secretOrKey,
                    {
                        expiresIn: 31556926 // 1 year in seconds
                    },
                    (err, token) => {
                        res.json({
                            success: true,
                            token: "Bearer " + token
                        });
                    }
                );
            } else {
                return res
                    .status(400)
                    .json({ passwordincorrect: "Password incorrect" });
            }
        });
    });
});


// @route POST api/users/add
// @desc Register user
// @access Public
router.post("/add", (req, res) => {
    // Form validation
    const { errors, isValid } = validateRegisterInput(req.body);
    // Check validation
    if (!isValid) {
        return res.status(400).json(errors);
    }
    User.findOne({ email: req.body.email }).then(user => {
        if (user) {
            return res.status(400).json({ email: "Email already exists" });
        } else {
            const newUser = new User({
                name: req.body.name,
                email: req.body.email,
                password: req.body.password
            });
            // Hash password before saving in database
            bcrypt.genSalt(10, (err, salt) => {
                bcrypt.hash(newUser.password, salt, (err, hash) => {
                    if (err) throw err;
                    newUser.password = hash;
                    newUser
                        .save()
                        .then(user => res.json(user))
                        .catch(err => console.log(err));
                });
            });
        }
    });
});




// @route POST api/users/desriptors
// @desc Register user
// @access Public
router.post("/descriptors", (req, res) => {

    const descriptor = new Descriptors({
        label: req.body.label,
        descriptors: req.body.descriptors
    });
    console.log(req.body.descriptors)
    User.findOne({ email: req.body.email }).then(user => {
        if (user) {
            return res.status(400).json({ email: "Email already exists" });
        } else {
            const descriptor = new Descriptors({
                label: req.body.label,
                descriptors: req.body.descriptors
            });
            descriptor
                .save()
                .then(desc => res.json(desc))
                .catch(err => console.log(err));

        }
    });
});

// @route PUT api/users/desriptors
// @desc Register user
// @access Public
router.put("/descriptors", (req, res) => {
    Descriptors.findOneAndUpdate({ label: req.body.label }, { descriptors: req.body.descriptors }).then(data => {
        if (!data) {
            res.status(404).send({
                message: `Cannot update Descriptor with name=${req.body.label}. Maybe Descriptor was not found!`
            });
        } else res.send({ message: "Descriptor was updated successfully." });
    });
});

// @route GET api/users/
// @desc get all users
// @access Private
router.get("/", (req, res) => {
    User.find({})
        .then(user => res.json(user))
        .catch(err => console.log(err));
});

// @route GET api/users/descriptors
// @desc get all descriptors
// @access Private
router.get("/descriptors", (req, res) => {
    Descriptors.find({})
        .then(descriptors => res.json(descriptors))
        .catch(err => console.log(err));
});


// @route GET api/users/
// @desc get user by id
// @access Private
router.get("/:id", (req, res) => {
    User.findOne({ _id: req.params.id })
        .then((user) => res.json(user))
        .catch((err) => console.log(err));
});

// @route PUT api/users/edit
// @desc edit user
// @access Private
router.put("/:id", (req, res) => {

    const id = req.params.id;
    bcrypt.genSalt(10, (err, salt) => {

        if (err) throw err;

        if (!req.body.password) {
            User.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
                .then(data => {
                    if (!data) {
                        res.status(404).send({
                            message: `Cannot update User with id=${id}. Maybe User was not found!`
                        });
                    } else res.send({ message: "User was updated successfully." });
                })
                .catch(err => {
                    res.status(500).send({
                        message: "Error updating User with id=" + id
                    });
                });
        }
        else {

            bcrypt.hash(req.body.password, salt, (err, hash) => {
                req.body.password = hash;
                User.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
                    .then(data => {
                        if (!data) {
                            res.status(404).send({
                                message: `Cannot update User with id=${id}. Maybe User was not found!`
                            });
                        } else res.send({ message: "User was updated successfully." });
                    })
                    .catch(err => {
                        res.status(500).send({
                            message: "Error updating User with id=" + id
                        });
                    });
            });
        }

    });
});
module.exports = router;