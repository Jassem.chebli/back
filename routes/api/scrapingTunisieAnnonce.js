const express = require("express");
const router = express.Router();
const keys = require("../../config/keys");
// Load Ad model
const Ad = require("../../models/Ad");
var request = require('request');
const cheerio = require('cheerio');

// @route Get api/ad/tunisieAnnonce/scraping
// @desc scraping data from Tunisie Anonnonce website from each category
// @access Private
router.get("/scraping/:type/:pageNb", (req, res) => {
    var type = 'AnnoncesTelephonie.asp'; //par defaut 
    if(req.params.type == 'mobile')
        type = 'AnnoncesTelephonie.asp'; 
    else if(req.params.type == 'technologie')
        type = 'AnnoncesInformatique.asp'; 

    request('http://www.tunisie-annonce.com/'+type+'?rech_page_num='+req.params.pageNb, function (error, response, html) {
    if (!error && response.statusCode == 200) {
        var $ = cheerio.load(html);
        $('tr.Tableau1').children('td').each(function(i, element){
          var pub = $(this);
          
          var url = pub.children('a').attr('href');
          //get each url of Ad for more information
          if(url != undefined && url.indexOf('DetailsAnnonceAutres.asp?cod_ann=') != -1)
            {
                request('http://www.tunisie-annonce.com/'+url, function (error, response, html) {
                    if (!error && response.statusCode == 200) {
                        var $ = cheerio.load(html);
                        var pub = $(this);
                        var category = '';
                        var adresse = '';
                        var price = '';
                        var content = '';
                        var date = ''
                        //get data ad 
                        $('.da_field_text').each(function(i, element){
                            var pub = $(this);
                            if(pub.prev().text().indexOf('Catégorie') != -1)
                                category = pub.text();
                            else if(pub.prev().text().indexOf('Localisation') != -1)
                                adresse = pub.text(); 
                            else if(pub.prev().text().indexOf('Adresse') != -1)
                                adresse = adresse + ' , ' +pub.text(); 
                            else if(pub.prev().text().indexOf('Prix') != -1)
                                price = pub.text(); 
                            else if(pub.prev().text().indexOf('Texte') != -1)
                                content = pub.text();     
                            else if(pub.prev().text().indexOf('Insérée le') != -1)
                                date = pub.text();   
                            else if(pub.prev().text().indexOf('Modifiée le') != -1)
                                date = pub.text();   
                                
                            if(pub.prev().text().indexOf('Texte') != -1)
                                console.log(pub.text());
                        })
                        console.log('************************************')

                        var mobile = $('.phone').children('span.da_contact_value').text();
                        console.log('Mobile : ===> '+mobile);
                        var cellphone = $('.cellphone').children('span.da_contact_value').text();
                        if(mobile != '')
                            console.log('Mobile : ===> '+mobile);
                        else{
                            console.log('Mobile : ===> '+cellphone);
                            mobile = cellphone;
                        }

                        var link = 'http://www.tunisie-annonce.com/'+url ;

                        var image = $('#PhotoMax_0').attr('src');
                        
                        Ad.findOne({ link: 'http://www.tunisie-annonce.com/'+url }).then(ad => {
                            if (ad) {
                                //add already exists so ignore it 
                            } else {
                                const newAd = new Ad({
                                    content: content ,
                                    category: category,
                                    price: price,
                                    photo: 'http://www.tunisie-annonce.com'+image ,
                                    date: date,
                                    adresse: adresse,
                                    isTreated: false,
                                    link: link ,
                                    isLead: true,
                                    telephone: mobile ,    
                                    email: '',
                                    facebookAcount: '',
                                    twitterAcount: '',
                                    intentnum:0
                                });
                                newAd
                                    .save()
                                    .then()//ad => res.json(ad))
                                    .catch()//err => console.log(err));
                            }
                        });
                    }
                }); 
            }
        });
    }

    });
    
});

// @route DELETE api/ad/tunisieAnnonce/delete
// @desc delete all add of tunisie-annonce website scraped data 
// @access Private
router.delete("/delete", (req, res) => {
    Ad.remove({})
    .then(ad => res.json(ad))
    .catch(err => console.log(err));
});

// @route DELETE api/ad/tunisieAnnonce/delete
// @desc delete add of tunisie-annonce website scraped data 
// @access Private
router.delete("/delete/:id", (req, res) => {
    Ad.remove({_id:req.params.id})
    .then(ad => res.json(ad))
    .catch(err => console.log(err));
});

// @route GET api/add/tunisieAnnonce/
// @desc get all ad of tunisie-annonce website scraped data
// @access Private
router.get("/", (req, res) => {
    Ad.find({})
                .then(ad => res.json(ad))
                .catch(err => console.log(err));
});
router.get("/filtreByCategory", (req, res) => {
    
    Ad.find({},function(err,docs){
              tab=[]
            nbiphone=0;
            nbsamsung=0;
            autre=0;
            nbhuawei=0;
            docs.forEach(element => {
               // console.log(element.category);
                if(element.category.includes('iPhone')) nbiphone++;
                else if(element.category.includes('Samsung')) nbsamsung++;
                else if(element.category.includes('Autre Marque')) autre++;
                else if(element.category.includes('Hwawei')) nbhuawei++;
            });
            var objiphone={
                marque:"Iphone",
                Occurence:nbiphone
            } 
            var objSumsung={
                marque:"Samsung",
                Occurence:nbsamsung
            }
            var objAutre={
                marque:"Autre Marque",
                Occurence:autre
            }
            tab.push(objiphone ,objSumsung,objAutre) 
           res.json(tab);
            
        });
        
});

router.get("/filtreByMonth", (req, res) => {
    
    Ad.find({},function(err,docs){
            tab=[]
            janv=0;
            fev=0;
            mars=0;
            avril=0;
            mai = 0 ; 
            juin = 0 ; 
            juillet = 0 ; 
            aout = 0 ; 
            sept = 0 ; 
            october = 0 ; 
            november = 0 ; 
            december = 0 ; 
            docs.forEach(element => {             
                if(element.date.includes('/01/')) janv++;
                else if(element.date.includes('/02/')) fev++;
                else if(element.date.includes('/03/')) mars++;
                else if(element.date.includes('/04/')) avril++;
                else if(element.date.includes('/05/')) mai++;
                else if(element.date.includes('/06/')) juin++;
                else if(element.date.includes('/07/')) juillet++;
                else if(element.date.includes('/08/')) aout++;
                else if(element.date.includes('/09/')) sept++;
                else if(element.date.includes('/10/')) october++;
                else if(element.date.includes('/11/')) november++;
                else if(element.date.includes('/12/')) december++;       
            });
          
            var Janvi={  mois:"Jan",  Occurence:janv    } 
            var Février={  mois:"févr",  Occurence:fev    } 
            var Mars={  mois:"mar",  Occurence:mars    } 
            var Avril={  mois:"Avr",  Occurence:avril    } 
            var Mai={  mois:"May",  Occurence: mai    } 
            var Juin={  mois:"Jun",  Occurence:juin    } 
            var Juillet={  mois:"Jul",  Occurence: juillet    } 
            var août={  mois:"Aug",  Occurence: aout    } 
            var septembre={  mois:"Sep",  Occurence:sept    } 
            var octobre={  mois:"Oct",  Occurence: october    } 
            var novembre={  mois:"Nov",  Occurence: november    } 
            var décembre={  mois:"Dec",  Occurence: december    } 
            tab.push(Janvi ,Février , Mars  ,Avril , Mai , Juin ,Juillet  , août , septembre , octobre , novembre , décembre  ) 
           res.json(tab);
            
        });
        
});

router.get("/filtreByRegion", (req, res) => {
    
    Ad.find({},function(err,docs){
              tabs=[] 
            nbArina=0;
            nbBeja=0;
            nbBenArous=0;
            nbBizert=0;
            nbGabes = 0 ; 
            nbGafsa = 0 ; 
            nbJendouba  = 0 ; 
            nbKairouan = 0 ; 
            nbKasserine = 0 ; 
            nbKbelli = 0;
            nbKef = 0;
            nbMahdia = 0;
            nbManouba = 0;
            nbMednine= 0;
            nbMonastir = 0 ; 
            nbNabeul=0 ; 
            nbSfax = 0; 
            nbSidiBouzid= 0;
            nbSiliana= 0;
            nbSousse = 0;
            nbTatouine = 0;
            nbTozeur = 0 ; 
            nbTunis = 0 ; 
            nbZaghouan = 0 ; 

            docs.forEach(element => {
               // console.log(element.category);
                if(element.adresse.includes('Ariana')) nbArina++;    
                else if(element.adresse.includes('Beja')) nbBeja++;
                else if(element.adresse.includes('Ben arous')) nbBenArous++;
                else if(element.adresse.includes('Bizerte')) nbBizert++;
                else if(element.adresse.includes('Gabes')) nbGabes++;
                else if(element.adresse.includes('Gafsa')) nbGafsa++;
                else if(element.adresse.includes('Jendouba')) nbJendouba++;
                else if(element.adresse.includes('Kairouan')) nbKairouan++;
                else if(element.adresse.includes('Kasserine')) nbKasserine++;
                else if(element.adresse.includes('Kebili')) nbKbelli++;
                else if(element.adresse.includes('Le Kef')) nbKef++;
                else if(element.adresse.includes('Mahdia'))  nbMahdia++;
                else if(element.adresse.includes('Manouba'))nbManouba ++;
                else if(element.adresse.includes('Medenine')) nbMednine++;
                else if(element.adresse.includes('Monastir')) nbMonastir++;
                else if(element.adresse.includes('Nabeul')) nbNabeul++;
                else if(element.adresse.includes('Sfax')) nbSfax++;
                else if(element.adresse.includes('Sidi bouzid')) nbSidiBouzid++;
                else if(element.adresse.includes('Siliana')) nbSiliana++;
                else if(element.adresse.includes('Sousse')) nbSousse++;
                else if(element.adresse.includes('Tataouine')) nbTatouine++;
                else if(element.adresse.includes('Tozeur')) nbTozeur++;
                else if(element.adresse.includes('Tunis')) nbTunis++;
                else if(element.adresse.includes('Zaghouan')) nbZaghouan++;

            });

            var objAriana={  marque:"Ariana",  Occurence:nbArina  } 
            var Beja={  marque:"Beja", Occurence:nbBeja  }
            var Benarous={  marque:"Ben arous",  Occurence:nbBenArous  } 
            var objBizerte={ marque:"Bizerte",  Occurence:nbBizert   } 
            var Gabes={   marque:"Gabes",   Occurence:nbGabes   }    
            var Gafsa={ marque:"Gafsa",  Occurence:nbGafsa }  
            var Jendouba={  marque:"Jendouba", Occurence:nbJendouba  } 
            var Kairouan={  marque:"Kairouan", Occurence:nbKairouan   } 
            var Kasserine = {    marque: "Kasserine",     Occurence: nbKasserine }
            var Kebili = {   marque:"Kebili",   Occurence:nbKbelli   } 
            var LeKef={  marque:"Le Kef",  Occurence:nbKef     } 
            var Mahdia={   marque:"Mahdia",  Occurence:nbMahdia    }  
            var Manouba={  marque:"Manouba",   Occurence:nbManouba  } 
            var Medenine={  marque:"Medenine", Occurence:nbMednine   } 
            var Monastir={  marque:"Monastir", Occurence:nbMonastir  } 
            var Nabeul={   marque:"Nabeul",  Occurence:nbNabeul    } 
            var Sfax={   marque:"Sfax",   Occurence:nbSfax  } 
            var Sidibouzid={  marque:"Sidi bouzid",  Occurence:nbSidiBouzid   } 
            var Siliana={    marque:"Siliana",  Occurence:nbSiliana  } 
            var Sousse={  marque:"Sousse", Occurence:nbSousse    } 
            var Tataouine={  marque:"Tataouine",  Occurence:nbTatouine   } 
            var Tozeur={  marque:"Tozeur",  Occurence:nbTozeur   } 
            var Tunis={  marque:"Tunis",  Occurence:nbTunis  } 
            var Zaghouan={  marque:"Zaghouan",  Occurence:nbZaghouan  } 
              
            tabs.push(objAriana,Beja,Benarous,objBizerte,Gabes , Gafsa , Jendouba , Kairouan , Kasserine , Kebili , 
                LeKef , Mahdia , Manouba  , Monastir , 
                Medenine, Nabeul , Sfax , Sidibouzid , Siliana , Sousse , Tataouine , Tozeur , Tunis , Zaghouan) ;
                tabs.sort(function (b,a) {
                    return a.Occurence - b.Occurence;
                  });
                  
                res.json(tabs.slice(0,5));          
        });      
});
// @route GET api/add/tunisieAnnonce/
// @desc update ad and change it to treated of tunisie-annonce website scraped data
// @access Private
router.put("/update/isTreated/:id/:isTreated", (req, res) => {
    Ad.updateMany({_id: req.params.id} , {$set :{isTreated : req.params.isTreated}})
    .then(ad => res.json(ad))
    .catch(err => console.log(err));
});

router.put("/update/:id/:intentnum", (req, res) => {
    Ad.updateMany({_id: req.params.id} , {$set :{intentnum : req.params.intentnum}})
        .then(ad => res.json(ad))
        .catch(err => console.log(err));
});

module.exports = router;