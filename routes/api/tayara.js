const express = require("express");
const router = express.Router();
const keys = require("../../config/keys");
const Ad = require("../../models/Ad");

const puppeteer = require('puppeteer');

// @route Get api/ad/tayara
// @desc scraping ads details from tayara.tn
// @access Private
router.get("/", (req, res) => {
    (async () => {
        let adURL = 'https://www.tayara.tn/listings/t%C3%A9l%C3%A9phones-31/5e020c3a-56d4-41f6-9dd3-47a15d54fbcf/samsung-a30';
        const browser = await puppeteer.launch();
        const page = await browser.newPage();
        await page.goto(adURL, { waitUntil: 'networkidle2' });
        let data = await page.evaluate(() => {
            let title = document.querySelector('h1[data-name="adview_title"]').innerText;
            let price = document.querySelector('h2 span[data-name="price"]').innerText;
            let date = document.querySelector('h1[data-name="adview_title"] + div p span:last-child').innerText;
            let place = document.querySelector('h2 p[data-name="adview_location"]').innerText;
            let category = document.querySelector('h2 p[data-name="adview_category"]').innerText;
            let image = document.querySelector('.gallery-item-active img').src;

            return {
                title, price, date, place, category, image
            }
        });
        console.log(data)
        await browser.close();
    })().then(newAd => res.json(newAd))
        .catch(err => console.log(err));
});
module.exports = router;