const express = require("express");
const router = express.Router();
const keys = require("../../config/keys");
const Ad = require("../../models/Ad");

const puppeteer = require('puppeteer');

// @route Get api/ad/tayara
// @desc scraping ads details from tayara.tn
// @access Private
router.get("/", (req, res) => {

    (async () => {
        let adURL = 'https://www.tayara.tn/listings/ordinateurs-portables-33/60297ecb-531d-4908-af93-dc41b72d008e/hp-pavilion-15-gaming-laptop';
        const browser = await puppeteer.launch(({ headless: true }));
        const page = await browser.newPage();
        page.setUserAgent('Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36')
        await page.goto(adURL, { waitUntil: 'networkidle2' });
        let data = await page.evaluate(() => {
            let button = document.querySelector('button#show_phone_number');
            button.click();
            let title = document.querySelector('h1[data-name="adview_title"]').innerText;
            let price = document.querySelector('h2 span[data-name="price"]').innerText;
            let date = document.querySelector('h1[data-name="adview_title"] + div p span:last-child').innerText;
            let place = document.querySelector('h2 p[data-name="adview_location"]').innerText;
            let category = document.querySelector('h2 p[data-name="adview_category"]').innerText;
            let image = document.querySelector('.gallery-item-active img').src;
            let telephone = document.querySelector('p[data-name="contact_phone"]').innerText;
            
            return {
                title,
                price,
                date,
                place,
                category,
                image,
                telephone
            }

        });
        const newAd = new Ad({
            content: data.title,
            category: data.category,
            price: data.price,
            photo: data.image,
            date: data.date,
            adresse: data.place,
            isTreated: false,
            link: adURL,
            isLead: false,
            telephone : data.telephone
        });

        await browser.close();

        return res.status(200).json(newAd)

    })();
});
module.exports = router;