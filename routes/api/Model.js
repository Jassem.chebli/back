const express = require("express");
const router = express.Router();
const keys = require("../../config/keys");
const Row = require("../../models/Row")
// Load Dictionnary model

// @route POST api/dictionnary/ajouterWord
// @desc add word to dictionnary
// @access Private
router.post("/ajouterRowBuyer/:content", (req, res) => {
    
            const row = new Row({
                text: req.params.content,
                                intent : "Buy"

            });
            row
                .save()
                .then(dictionnary => res.json(dictionnary))
                .catch(err => console.log(err));
        
});
router.post("/ajouterRowNotBuyer/:content", (req, res) => {
    console.log(req.body)
    const row = new Row({
        text: req.params.content,
        intent : "no"

    });
    row.save()
        .then(dictionnary => res.json(dictionnary))
        .catch(err => console.log(err));

});

router.get("/", (req, res) => {
    Row.find({})
        .then(dictionnary => res.json(dictionnary))
        .catch(err => console.log(err));
});

router.delete("/", (req, res) => {
    Row.remove({})
        .then(dictionnary => res.json(dictionnary))
        .catch(err => console.log(err));
});
router.delete("/:id", (req, res) => {
    Row.remove({_id:req.params.id})
        .then(dictionnary => res.json(dictionnary))
        .catch(err => console.log(err));
});

module.exports = router;