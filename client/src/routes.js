/*!

=========================================================
* Argon Dashboard React - v1.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import Index from "views/Index.js";
import List from "components/Dictionnary/List.js";
import Ad from "components/Scraping/Ad.js";
import Profile from "views/examples/Profile.js";
import Maps from "views/examples/Maps.js";
import Register from "components/Auth/Register.js";
import Login from "components/Auth/Login.js";
import Tables from "views/examples/Tables.js";
import Icons from "views/examples/Icons.js";

var routes = [
  {
    path: "/index",
    name: "Dashboard",
    icon: "ni ni-tv-2 text-primary",
    component: Index,
    layout: "/admin"
  },
  {
    path: "/ad",
    name: "Ad",
    icon: "ni ni-single-copy-04 text-orange",
    component: Ad,
    layout: "/admin"
  },
  {
    path: "/dictionnary",
    name: "Dictionnary",
    icon: "ni ni-caps-small text-info",
    component: List,
    layout: "/admin"
  }
];
export default routes;
