import React from "react";
// node.js library that concatenates classes (strings)
import classnames from "classnames";
import axios from 'axios';

import jwt_decode from "jwt-decode";
import setAuthToken from "../../utils/setAuthToken";
import { setCurrentUser, logoutUser } from "../../actions/authActions";
import store from "../../store";


// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  NavItem,
  NavLink,
  Nav,
  Progress,
  Table,
  Container,
  Row,
  Col,
  UncontrolledTooltip
} from "reactstrap";

// reactstrap components
import {
  DropdownMenu,
  DropdownItem,
  UncontrolledDropdown,
  DropdownToggle,
  Form,
  FormGroup,
  InputGroupAddon,
  InputGroupText,
  Input,
  InputGroup,
  Navbar,
  Media,
  CardFooter,
  Pagination,
  PaginationItem,
  PaginationLink,
  Alert
} from "reactstrap";


import Header from "components/Headers/Header.js";
import Ad from "layouts/Ad";

// Check for token to keep user logged in
if (localStorage.jwtToken) {
  // Set auth token header auth
  const token = localStorage.jwtToken;
  setAuthToken(token);
  // Decode token and get user info and exp
  const decoded = jwt_decode(token);
  // Set user and isAuthenticated
  store.dispatch(setCurrentUser(decoded));
  // Check for expired token
  const currentTime = Date.now() / 1000; // to get in milliseconds
  if (decoded.exp < currentTime) {
    // Logout user
    store.dispatch(logoutUser());

    // Redirect to login
    window.location.href = "./login";
  }
}


let prev = 0;
let next = 0;
let last = 0;
let first = 0;


class List extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activeNav: 1,
      chartExample1Data: "data1",
      ad: [],
      showAlertAdd: false,
      showAlertDel: false,
      showAlertUpd: false,
      currentPage: 1,
      todosPerPage: 6,
    };


    this.handleClick = this.handleClick.bind(this);
    this.handleLastClick = this.handleLastClick.bind(this);
    this.handleFirstClick = this.handleFirstClick.bind(this);
  }
  toggleNavs = (e, index) => {
    e.preventDefault();
    this.setState({
      activeNav: index,
      chartExample1Data:
        this.state.chartExample1Data === "data1" ? "data2" : "data1"
    });
  };

  //show list of keywords when component is ready 
  componentDidMount() {
    this.GetScrapedData();

  }

  GetScrapedData() {
    axios.get('/api/ad/tunisieAnnonce/')
      .then(res => {
        console.log('list ad', res.data);
        const ad = res.data;
        this.setState({ ad: ad });
      })
  }

  ScrapingData = () => {
    axios.get('/api/ad/tunisieAnnonce/scraping/mobile/1')
      .then(res => {
        console.log(res);
        console.log(res.data);
        this.GetScrapedData();
      })
      .catch(err => { console.log(err) });
  }


  UpdateTreated = (Ad) => {
    axios.put('/api/ad/tunisieAnnonce/update/isTreated/' + Ad._id + '/' + !Ad.isTreated)
      .then(res => {
        console.log(res);
        console.log(res.data);
        this.GetScrapedData();

        this.setState({
          showAlertUpd: true,
        });

        setInterval(() => {
          this.setState({
            showAlertUpd: false,
          });
        }, 4000);
      })
  }

  DeleteScrapingData = (Ad) => {
    axios.delete('/api/ad/tunisieAnnonce/delete/' + Ad._id)
      .then(res => {
        console.log(res);
        console.log(res.data);
        this.GetScrapedData();

        this.setState({
          showAlertDel: true,
        });

        setInterval(() => {
          this.setState({
            showAlertDel: false,
          });
        }, 4000);
      })
  }

  DeleteAll() {
    axios.delete('/api/ad/tunisieAnnonce/delete/')
      .then(res => {
        console.log(res);
        console.log(res.data);
        this.GetScrapedData();

        this.setState({
          showAlertDel: true,
        });

        setInterval(() => {
          this.setState({
            showAlertDel: false,
          });
        }, 4000);
      })
  }

  VisitAd = (Ad) => {
    window.open(Ad.link);
  }

  showImage = (img) => {
    window.open(img);
  }

  Contact = (data) => {
    axios.post('/api/contact/sms/' + data.mobile)
      .then(res => {
        console.log(res);
        console.log(res.data);
      })
  }

  handleClick(event) {
    event.preventDefault();
    this.setState({
      currentPage: Number(event.target.id)
    });
  }

  handleLastClick(event) {
    event.preventDefault();
    this.setState({
      currentPage: last
    });
  }
  handleFirstClick(event) {
    event.preventDefault();
    this.setState({
      currentPage: 1
    });
  }


  render() {

    let { ad, currentPage, todosPerPage } = this.state;

    // Logic for displaying current todos
    let indexOfLastTodo = currentPage * todosPerPage;
    let indexOfFirstTodo = indexOfLastTodo - todosPerPage;
    let currentAd = ad.slice(indexOfFirstTodo, indexOfLastTodo);
    prev = currentPage > 0 ? (currentPage - 1) : 0;
    last = Math.ceil(ad.length / todosPerPage);
    next = (last === currentPage) ? currentPage : currentPage + 1;

    // Logic for displaying page numbers
    let pageNumbers = [];
    for (let i = 1; i <= last; i++) {
      pageNumbers.push(i);
    }

    return (
      <>
        <Header />
        {/* Page content */}
      <br></br>
        <Alert color="success" hidden={!this.state.showAlertAdd}>
          <strong>Success!</strong> Data was added successfully.
        </Alert>
        <Alert color="danger" hidden={!this.state.showAlertDel}>
          <strong>Success!</strong> Data was deleted successfully.
        </Alert>
        <Alert color="info" hidden={!this.state.showAlertUpd}>
          <strong>Success!</strong> Data was updated successfully.
        </Alert>
        <br></br> <br></br>
        <Container className="mt--7" fluid>
          <Row className="mt-5">
            <Col className="mb-5 mb-xl-0" xl="12">
              <Card className="shadow">
                <CardHeader className="border-0">
                  <Row className="align-items-center">
                    <div className="col">
                      <h3 className="mb-0">List of Scraping Ad :</h3>
                    </div>
                    <div className="col text-right" >
                      <Form className="navbar-search  form-inline mr-3 d-none d-md-flex ml-lg-auto">
                        <Button style={{ marginLeft: '60%' }}
                          color="primary"
                          onClick={e => this.ScrapingData()}
                          size="sm">
                          Scraping
                        </Button>
                        <Button
                          color="danger"
                          onClick={e => this.DeleteAll()}
                          size="sm">
                          Delete All
                        </Button>
                      </Form>
                    </div>
                  </Row>
                </CardHeader>
                <Table className="align-items-center table-flush" responsive>
                  <thead className="thead-light">
                    <tr>
                      <th scope="col">Description</th>
                      <th scope="col">Category</th>
                      <th scope="col">Price</th>
                      <th scope="col">Adresse</th>
                      <th scope="col">Action</th>
                    </tr>
                  </thead>
                  <tbody>

                    {currentAd.map(data => {
                      if (data.isTreated) {
                        return (<tr key={data._id} style={{ backgroundColor: '#b3d9ff' }}>
                          <td scope="col"> <textarea style={{ borderColor: '#b3d9ff', backgroundColor: '#b3d9ff', color: '#525f7f' }} value={data.content} contentEditable='false' cols='35' rows={(data.content.length + 36) / 36}></textarea></td>
                          <td scope="col"> {data.category}</td>
                          <td scope="col"> {data.price.replace('Dinar Tunisien', '')}</td>
                          <td scope="col"> <textarea style={{ borderColor: '#b3d9ff', backgroundColor: '#b3d9ff', color: '#525f7f' }} value={data.adresse} contentEditable='false' cols='20' rows={(data.adresse.length + 40) / 27}></textarea></td>
                          <td style={{ maxWidth: '10px', minWidth: '10px', width: '10px' }}>
                            <UncontrolledDropdown>
                              <DropdownToggle
                                className="btn-icon-only text-light"
                                href="#pablo"
                                role="button"
                                size="sm"
                                color="black"
                                onClick={e => e.preventDefault()}
                              >
                                <i className="fas fa-ellipsis-v" style={{ color: '#999999' }} />
                              </DropdownToggle>
                              <DropdownMenu className="dropdown-menu-arrow" right>
                                <DropdownItem
                                  href="#pablo"
                                  onClick={e => this.UpdateTreated(data)}
                                >
                                  Switch Treated
                            </DropdownItem>
                                <DropdownItem
                                  href="#pablo"
                                  onClick={e => this.VisitAd(data)}
                                >
                                  Visit
                            </DropdownItem>
                                <DropdownItem
                                  href="#pablo"
                                  onClick={e => this.Contact(data)}
                                >
                                  Contact
                            </DropdownItem>
                                <DropdownItem
                                  href="#pablo"
                                  onClick={e => this.DeleteScrapingData(data)}
                                >
                                  Delete
                            </DropdownItem>
                              </DropdownMenu>
                            </UncontrolledDropdown>
                          </td>
                        </tr>)
                      } else {
                        return (<tr key={data._id}>
                          <td scope="col" style={{ width: "50px", height: "50px" }}> <textarea style={{ borderColor: 'white', color: '#525f7f' }} value={data.content} contentEditable='false' cols='35' rows={(data.content.length + 36) / 36}></textarea></td>
                          <td scope="col"> {data.category}</td>
                          <td scope="col"> {data.price.replace('Dinar Tunisien', '')}</td>
                          <td scope="col"> <textarea style={{ borderColor: 'white', backgroundColor: 'white', color: '#525f7f' }} value={data.adresse} contentEditable='false' cols='20' rows={(data.adresse.length + 40) / 27}></textarea></td>
                          <td>
                            <UncontrolledDropdown>
                              <DropdownToggle
                                className="btn-icon-only text-light"
                                href="#pablo"
                                role="button"
                                size="sm"
                                color=""
                                onClick={e => e.preventDefault()}
                              >
                                <i className="fas fa-ellipsis-v" />
                              </DropdownToggle>
                              <DropdownMenu className="dropdown-menu-arrow" right>
                                <DropdownItem
                                  href="#pablo"
                                  onClick={e => this.UpdateTreated(data)}
                                >
                                  Switch Treated
                            </DropdownItem>
                                <DropdownItem
                                  href="#pablo"
                                  onClick={e => this.VisitAd(data)}
                                >
                                  Visit
                            </DropdownItem>
                                <DropdownItem
                                  href="#pablo"
                                  onClick={e => this.Contact(data)}
                                >
                                  Contact
                            </DropdownItem>
                                <DropdownItem
                                  href="#pablo"
                                  onClick={e => this.DeleteScrapingData(data)}
                                >
                                  Delete
                            </DropdownItem>
                              </DropdownMenu>
                            </UncontrolledDropdown>
                          </td>
                        </tr>)
                      }
                    }


                    )
                    }
                  </tbody>
                </Table>
                <CardFooter className="py-4">
                  <ul id="page-numbers">
                    <nav>
                      <Pagination className="pagination justify-content-center"
                        listClassName="justify-content-center">
                        <PaginationItem>
                          {prev === 0 ? <PaginationLink disabled><i className="fa fa-angle-left" /></PaginationLink> :
                            <PaginationLink onClick={this.handleClick} id={prev} href={prev}> <i className="fa fa-angle-left" onClick={this.handleClick} id={prev} href={prev} /> </PaginationLink>
                          }
                        </PaginationItem>
                        {
                          pageNumbers.map((number, i) =>
                            <Pagination key={i}>
                              <PaginationItem active={pageNumbers[currentPage - 1] === (number) ? true : false} >
                                <PaginationLink onClick={this.handleClick} href={number} key={number} id={number}>
                                  {number}
                                </PaginationLink>
                              </PaginationItem>
                            </Pagination>
                          )}

                        <PaginationItem>
                          {
                            currentPage === last ? <PaginationLink disabled><i className="fa fa-angle-right" disabled /></PaginationLink> :
                              <PaginationLink onClick={this.handleClick} id={pageNumbers[currentPage]} href={pageNumbers[currentPage]}><i className="fa fa-angle-right" onClick={this.handleClick} id={pageNumbers[currentPage]} href={pageNumbers[currentPage]} /></PaginationLink>
                          }
                        </PaginationItem>
                      </Pagination>
                    </nav>
                  </ul>
                </CardFooter>
              </Card>
            </Col>
          </Row>
        </Container>
      </>
    );
  }
}

export default List;
