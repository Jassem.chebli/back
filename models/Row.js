const mongoose = require("mongoose");
const Schema = mongoose.Schema;
// Create Schema
const RowSchema = new Schema({
    text: {
        type: String,
        required: false
    },
    intent :  {
        type: String,
        required: false
    },
});
module.exports = Row = mongoose.model("Row", RowSchema);