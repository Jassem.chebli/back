const mongoose = require("mongoose");
const Schema = mongoose.Schema;
// Create Schema
const AdSchema = new Schema({
    content: {
        type: String,
        required: false
    },
    category: {
        type: String,
        required: false
    },
    price: {
        type: String,
        required: false
    },
    photo: {
        type: String,
        required: false
    },
    date: {
        type: String,
        required: false
    },
    adresse: {
        type: String,
        required: false
    },
    isTreated: {
        type: Boolean,
        required: false
    },
    link: {
        type: String,
        required: false
    },
    isLead: {
        type: Boolean,
        required: false
    },
    telephone: {
        type: String,
        required: false
    },    
    email: {
        type: String,
        required: false
    },
    facebookAcount: {
        type: String,
        required: false
    },
    twitterAcount: {
        type: String,
        required: false
    },

    intentnum :  {
        type:Number,
        required: false
    },
});
module.exports = Ad = mongoose.model("Ad", AdSchema);