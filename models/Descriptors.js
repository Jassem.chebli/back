const mongoose = require("mongoose");
const Schema = mongoose.Schema;
// Create Schema
const DescriptorsSchema = new Schema({
    label: {
        type: String,
        required: true
    },
    descriptors: {
        type: String,
        required: true
    }
});
module.exports = Descriptors = mongoose.model("Descriptors", DescriptorsSchema);