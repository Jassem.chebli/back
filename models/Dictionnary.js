const mongoose = require("mongoose");
const Schema = mongoose.Schema;
// Create Schema
const DictionnairySchema = new Schema({
    word: {
        type: String,
        required: true
    },
    synonyme: {
        type: Array,
        required: true
    }
});
module.exports = Dictionnary = mongoose.model("Dictionnary", DictionnairySchema);