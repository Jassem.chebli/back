const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const passport = require("passport");
const path = require('path');

const users = require("./routes/api/users");
const dictionnary = require("./routes/api/dictionnary");
const contact = require("./routes/api/contact");
const add = require("./routes/api/scrapingTunisieAnnonce");
const scrapTayara = require("./routes/api/tayaraWebScraping");
const AdFb = require("./routes/api/AdFb");
const Model = require("./routes/api/Model");

var cors = require('cors');
const app = express();
// Bodyparser middleware
app.use(
  bodyParser.urlencoded({
    extended: false
  })
);
app.use(bodyParser.json());

//to fix problem of No 'Access-Control-Allow-Origin'
app.use(cors());

app.use((req, res, next) => {
  res.set({
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE',
    'Access-Control-Allow-Headers': 'Content-Type'
  })
  next();
})

// DB Config
const db = require("./config/keys").mongoURI;
// Connect to MongoDB
mongoose
  .connect(
     db,
    {
      useNewUrlParser: true,
      useUnifiedTopology: true
    }
  )
  .then(() => console.log("MongoDB successfully connected"))
  .catch(err => console.log(err));

// Passport middleware
app.use(passport.initialize());
// Passport config
require("./config/passport")(passport);
// Routes
app.use("/api/users", users);
app.use("/api/dictionnary", dictionnary);
app.use("/api/contact", contact);
app.use("/api/ad/tunisieAnnonce", add);
app.use("/api/ad/tayara", scrapTayara);
app.use("/api/ad/facebook",AdFb)
app.use("/api/model", Model);

if (process.env.NODE_ENV === 'production') {
  app.use(express.static( 'client/build' ));

  app.get('*', (req, res) => {
      res.sendFile(path.join(__dirname, 'client', 'build', 'index.html')); // relative path
  });
}

const port = process.env.PORT || 5000; // process.env.port is Heroku's port if you choose to deploy the app there
app.listen(port, () => console.log(`Server up and running on port ${port} !`));